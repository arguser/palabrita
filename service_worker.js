self.addEventListener('install', function (e) {
    e.waitUntil(
        caches.open('palabritaapp').then(function (cache) {
            return cache.addAll([
                '/index.html',
                '/manifest.json',
                '/main.js',
                '/palabrita_og.png',
                '/images/palabrita_32x32.png',
                '/images/palabrita_192x192.png',
                '/images/palabrita_512x512.png',
            ]);
        })
    );
});

self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    );
});

