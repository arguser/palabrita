port module Main exposing (CharStatus(..), Configuration, Events, Flags, GameState, GameStatus(..), KeyboardKeys, Model, Msg(..), Screen(..), Statistics, backspaceButton, charStatusChar, chartView, checkGameStatus, compareCharStatus, configView, copy, device, enterButton, gameBoardView, gameRow, gameView, getCurrentSecret, getDaysFromStartDate, getHoursToNextGame, getMinutesToNextGame, getSecondsToNextGame, getTime, getTimeStringToNextGame, getUpdate, guessListsStorage, handleCorrectDuplicates, handleMisplacedDuplicates, howToView, init, initModel, initStatistics, initializeKeyboard, isWordValid, keyDecoder, keyStatusColor, keyboardView, letterIs, main, mapChars, randomSeed, sanitizeChar, sanitizedList, setKeyStatus, setStorage, shareText, statsGuesses, statsView, storageGuessList, subscriptions, tile, toKey, toggleCheckboxWidget, tooltip, update, updateView, validateGuess, view, winToastText, withTooltip)

import Array exposing (Array, initialize)
import Browser exposing (Document)
import Browser.Events exposing (Visibility(..))
import Browser.Navigation
import Chart as C
import Chart.Attributes as CA
import Colors exposing (absentColor, bgColor, correctColor, correctContrastColor, fgColor, hiddenColor, keyboardColor, presentColor, presentContrastColor, specialKeyColor)
import Date
import Element exposing (Color, Device, DeviceClass(..), Element, Orientation(..), alignBottom, alignLeft, alignRight, alignTop, centerX, centerY, classifyDevice, column, el, fill, fillPortion, height, htmlAttribute, inFront, link, maximum, minimum, moveDown, moveRight, moveUp, newTabLink, padding, paddingEach, paddingXY, paragraph, px, row, spaceEvenly, spacing, text, transparent, width, wrappedRow)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Lazy
import Html exposing (Html)
import Html.Attributes as HA
import Http
import Json.Decode exposing (index, string)
import Random exposing (Seed)
import String exposing (fromChar)
import Svg as S
import Task
import Time exposing (Month(..))
import Url
import Words exposing (words)


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = DoNothingUrl
        , onUrlRequest = DoNothingRequest
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


init : Flags -> Url.Url -> Browser.Navigation.Key -> ( Model, Cmd Msg )
init flags _ _ =
    ( initModel flags, Task.perform Zone Time.here )



-- PORT


port setStorage : GameState -> Cmd msg


port copy : String -> Cmd msg


initModel : Flags -> Model
initModel flags =
    { currectTime = Time.millisToPosix 0
    , currentDate = Nothing
    , events = NoEvent
    , game = 0
    , gameState = flags.gameState
    , gameStatus = Ongoing
    , guessIndex = 0
    , guessList = initialize 6 (always (initialize 5 (always (Hidden ' '))))
    , height = flags.height
    , intent = 0
    , keyboard = initializeKeyboard
    , screen =
        if flags.gameState.firstTime then
            HowTo

        else
            Game
    , secret = ""
    , startDate = Date.fromCalendarDate 2022 Feb 1
    , statistics = initStatistics
    , updatedAt = 0
    , width = flags.width
    , zone = Time.utc
    }



-- HTTP


getUpdate : Cmd Msg
getUpdate =
    Http.get
        { expect = Http.expectString GotUpdate
        , url = "./lastUpdate.txt"
        }



-- TIME


getTime : Cmd Msg
getTime =
    Task.perform CheckDate Time.now



-- getCurrentDateText : Maybe Date.Date -> String
-- getCurrentDateText currentDate =
--     Maybe.withDefault "" <| Maybe.map (format "dd/MM/yyyy") currentDate


getDaysFromStartDate : Model -> Date.Date -> Int
getDaysFromStartDate model date =
    Date.diff
        Date.Days
        model.startDate
        date


getTimeStringToNextGame : Time.Zone -> Time.Posix -> String
getTimeStringToNextGame zone time =
    getHoursToNextGame zone time ++ ":" ++ getMinutesToNextGame zone time ++ ":" ++ getSecondsToNextGame zone time


getHoursToNextGame : Time.Zone -> Time.Posix -> String
getHoursToNextGame zone time =
    let
        hours : Int
        hours =
            23 - Time.toHour zone time
    in
    if hours < 10 then
        "0" ++ String.fromInt hours

    else
        String.fromInt hours


getMinutesToNextGame : Time.Zone -> Time.Posix -> String
getMinutesToNextGame zone time =
    let
        minutes : Int
        minutes =
            59 - Time.toMinute zone time
    in
    if minutes < 10 then
        "0" ++ String.fromInt minutes

    else
        String.fromInt minutes


getSecondsToNextGame : Time.Zone -> Time.Posix -> String
getSecondsToNextGame zone time =
    let
        seconds : Int
        seconds =
            59 - Time.toSecond zone time
    in
    if seconds < 10 then
        "0" ++ String.fromInt seconds

    else
        String.fromInt seconds



-- DEVICE


device : Int -> Int -> Device
device width height =
    classifyDevice
        { height = height
        , width = width
        }



-- RANDOM


randomSeed : Array String -> Random.Seed -> ( Int, Random.Seed )
randomSeed words seed =
    Random.step (Random.int 0 (Array.length words)) seed



-- KEY EVENTS


keyDecoder : Json.Decode.Decoder Msg
keyDecoder =
    Json.Decode.map toKey (Json.Decode.field "key" Json.Decode.string)


toKey : String -> Msg
toKey string =
    case String.uncons string of
        Just ( char, "" ) ->
            if char == ' ' then
                DoNothing

            else
                Character char

        _ ->
            case string of
                "Enter" ->
                    Enter

                "Backspace" ->
                    Backspace

                _ ->
                    DoNothing



-- MODEL


type alias Model =
    { currectTime : Time.Posix, currentDate : Maybe Date.Date, events : Events, game : Int, gameState : GameState, gameStatus : GameStatus, guessIndex : Int, guessList : Array (Array CharStatus), height : Int, intent : Int, keyboard : KeyboardKeys, screen : Screen, secret : String, startDate : Date.Date, statistics : Statistics, updatedAt : Int, width : Int, zone : Time.Zone }


type alias Flags =
    { gameState : GameState, height : Int, width : Int }


type alias KeyboardKeys =
    { firstRow : Array CharStatus
    , secondRow : Array CharStatus
    , thirdRow : Array CharStatus
    }


type alias Statistics =
    { averageGuesses : Int, currentStreak : Int, gamesPlayed : Int, gamesWon : Int, guesses : List { asserts : Float }, maxStreak : Int, winPercentage : Int }


type alias Configuration =
    { colorBlind : Bool, darkMode : Bool, hardMode : Bool }


type alias GameState =
    { configuration : Configuration, firstTime : Bool, gameNum : Int, guessList : List String, intent : Int, lastUpdate : Int, statistics : Statistics }


type Events
    = NoEvent
    | ShowToast String
    | UpdateNeeded


type Screen
    = Config
    | Game
    | HowTo
    | Stats


type GameStatus
    = Lose
    | Ongoing
    | Win


type CharStatus
    = Absent Char
    | Correct Char
    | Hidden Char
    | Present Char


type Msg
    = Backspace
    | CancelUpdate
    | Character Char
    | CheckDate Time.Posix
    | Copy
    | DoNothing
    | DoNothingRequest Browser.UrlRequest
    | DoNothingUrl Url.Url
    | Enter
    | GotUpdate (Result Http.Error String)
    | HideToast Time.Posix
    | OnVisibilityChange Visibility
    | ShowConfiguration
    | ShowGame
    | ShowHowTo
    | ShowStatistics
    | TickTick Time.Posix
    | ToggleColorBlind
    | ToggleDarkMode
    | ToggleHardMode
    | Update
    | WindowResized Int Int
    | Zone Time.Zone


initializeKeyboard : KeyboardKeys
initializeKeyboard =
    { firstRow = Array.map (\c -> Hidden c) <| Array.fromList [ 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p' ]
    , secondRow = Array.map (\c -> Hidden c) <| Array.fromList [ 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ñ' ]
    , thirdRow = Array.map (\c -> Hidden c) <| Array.fromList [ 'z', 'x', 'c', 'v', 'b', 'n', 'm' ]
    }


initStatistics : Statistics
initStatistics =
    { averageGuesses = 0
    , currentStreak = 0
    , gamesPlayed = 0
    , gamesWon = 0
    , guesses = List.repeat 6 { asserts = 0 }
    , maxStreak = 0
    , winPercentage = 0
    }



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Backspace ->
            if model.guessIndex > 0 then
                ( { model
                    | guessIndex = model.guessIndex - 1
                    , guessList = Array.set model.intent (Array.set (model.guessIndex - 1) (Hidden ' ') (Maybe.withDefault Array.empty (Array.get model.intent model.guessList))) model.guessList
                  }
                , Cmd.none
                )

            else
                ( model, Cmd.none )

        CancelUpdate ->
            ( { model | gameStatus = Ongoing }, Cmd.none )

        Character a ->
            if model.guessIndex < 5 && model.gameStatus == Ongoing then
                ( { model
                    | guessIndex = model.guessIndex + 1
                    , guessList = Array.set model.intent (Array.set model.guessIndex (Hidden a) (Maybe.withDefault Array.empty (Array.get model.intent model.guessList))) model.guessList
                  }
                , Cmd.none
                )

            else
                ( model, Cmd.none )

        CheckDate newDate ->
            let
                lastUpdate : Int
                lastUpdate =
                    if model.gameState.lastUpdate == 0 then
                        Time.posixToMillis newDate

                    else
                        model.gameState.lastUpdate

                gameNum : Int
                gameNum =
                    getDaysFromStartDate model (Date.fromPosix model.zone newDate)

                refresh : Bool
                refresh =
                    gameNum /= model.gameState.gameNum

                index : ( Int, Seed )
                index =
                    randomSeed words (Random.initialSeed gameNum)

                currentSecret : String
                currentSecret =
                    getCurrentSecret (Tuple.first index) words

                storedGuess : Array (Array CharStatus)
                storedGuess =
                    if List.length model.gameState.guessList > 0 && not refresh then
                        storageGuessList model.gameState.guessList currentSecret

                    else
                        initialize 6 (always (initialize 5 (always (Hidden ' '))))

                storedIntent : Int
                storedIntent =
                    if refresh then
                        0

                    else
                        model.gameState.intent

                storedKeyboard : KeyboardKeys
                storedKeyboard =
                    if refresh then
                        model.keyboard

                    else
                        setKeyStatus storedGuess model.keyboard

                storedStats : Statistics
                storedStats =
                    model.gameState.statistics

                gameResolve : GameStatus
                gameResolve =
                    checkGameStatus
                        (if storedIntent > 0 then
                            storedIntent - 1

                         else
                            0
                        )
                        (Maybe.withDefault Array.empty
                            (Array.get
                                (if storedIntent > 0 then
                                    storedIntent - 1

                                 else
                                    0
                                )
                                storedGuess
                            )
                        )

                showStats : Screen
                showStats =
                    if gameResolve /= Ongoing then
                        Stats

                    else if model.gameState.firstTime then
                        HowTo

                    else
                        Game
            in
            ( { model
                | currectTime = newDate
                , currentDate = Just (Date.fromPosix model.zone newDate)
                , game = gameNum
                , gameState = { configuration = model.gameState.configuration, firstTime = False, gameNum = gameNum, guessList = guessListsStorage storedGuess, intent = storedIntent, lastUpdate = lastUpdate, statistics = storedStats }
                , gameStatus = gameResolve
                , guessIndex = 0
                , guessList = storedGuess
                , intent = storedIntent
                , keyboard = storedKeyboard
                , screen = showStats
                , secret = currentSecret
                , statistics = storedStats
              }
            , Cmd.batch
                [ setStorage
                    { configuration = model.gameState.configuration
                    , firstTime = False
                    , gameNum = gameNum
                    , guessList = guessListsStorage storedGuess
                    , intent = storedIntent
                    , lastUpdate = lastUpdate
                    , statistics = storedStats
                    }
                , getUpdate
                ]
            )

        Copy ->
            ( { model | events = ShowToast "Copiado!" }, copy <| shareText model.gameStatus model.game model.intent model.guessList )

        DoNothing ->
            ( model, Cmd.none )

        DoNothingRequest _ ->
            ( model, Cmd.none )

        DoNothingUrl _ ->
            ( model, Cmd.none )

        Enter ->
            if Array.length (Maybe.withDefault Array.empty (Array.get model.intent model.guessList)) == 5 && model.gameStatus == Ongoing then
                if isWordValid (Maybe.withDefault Array.empty (Array.get model.intent model.guessList)) then
                    let
                        resolvedGuessList : Array (Array CharStatus)
                        resolvedGuessList =
                            Array.set model.intent (validateGuess model.secret (Maybe.withDefault Array.empty (Array.get model.intent model.guessList))) model.guessList

                        gameResolve : GameStatus
                        gameResolve =
                            checkGameStatus model.intent (Maybe.withDefault Array.empty (Array.get model.intent resolvedGuessList))

                        newStats : Statistics
                        newStats =
                            let
                                stats : Statistics
                                stats =
                                    model.statistics
                            in
                            case gameResolve of
                                Lose ->
                                    { stats
                                        | currentStreak = 0
                                        , gamesPlayed = stats.gamesPlayed + 1
                                        , winPercentage = (stats.gamesWon * 100) // (stats.gamesPlayed + 1)
                                    }

                                Win ->
                                    { stats
                                        | currentStreak = stats.currentStreak + 1
                                        , gamesPlayed = stats.gamesPlayed + 1
                                        , gamesWon = stats.gamesWon + 1
                                        , guesses = statsGuesses model.intent stats.guesses
                                        , maxStreak =
                                            if stats.maxStreak > stats.currentStreak + 1 then
                                                stats.maxStreak

                                            else
                                                stats.currentStreak + 1
                                        , winPercentage = ((stats.gamesWon + 1) * 100) // (stats.gamesPlayed + 1)
                                    }

                                _ ->
                                    model.statistics
                    in
                    ( { model
                        | events =
                            if gameResolve == Win then
                                ShowToast <| winToastText model.intent

                            else if gameResolve == Lose then
                                ShowToast "Por poco! 😵"

                            else
                                model.events
                        , gameState =
                            { configuration = model.gameState.configuration
                            , firstTime = False
                            , gameNum = model.game
                            , guessList = guessListsStorage model.guessList
                            , intent = model.intent + 1
                            , lastUpdate = model.gameState.lastUpdate
                            , statistics = newStats
                            }
                        , gameStatus = gameResolve
                        , guessIndex = 0
                        , guessList = resolvedGuessList
                        , intent = model.intent + 1
                        , keyboard = setKeyStatus resolvedGuessList model.keyboard
                        , statistics = newStats
                      }
                    , setStorage
                        { configuration = model.gameState.configuration
                        , firstTime = False
                        , gameNum = model.game
                        , guessList = guessListsStorage model.guessList
                        , intent = model.intent + 1
                        , lastUpdate = model.gameState.lastUpdate
                        , statistics = newStats
                        }
                    )

                else
                    ( { model | events = ShowToast "Palabra Inválida 😵\u{200D}💫" }, Cmd.none )

            else
                ( model, Cmd.none )

        GotUpdate result ->
            case result of
                Err _ ->
                    ( model, Cmd.none )

                Ok response ->
                    let
                        updateTime : Int
                        updateTime =
                            Maybe.withDefault 0 (String.toInt response)

                        updateNeeded : Bool
                        updateNeeded =
                            model.gameState.lastUpdate < updateTime
                    in
                    ( { model
                        | events =
                            if updateNeeded then
                                UpdateNeeded

                            else
                                model.events
                        , updatedAt = updateTime
                      }
                    , Cmd.none
                    )

        HideToast _ ->
            let
                intent : Int
                intent =
                    if model.intent == 0 then
                        model.intent

                    else
                        model.intent - 1

                gameResolve : GameStatus
                gameResolve =
                    checkGameStatus intent (Maybe.withDefault Array.empty (Array.get intent model.guessList))

                showStats : Screen
                showStats =
                    if gameResolve /= Ongoing then
                        Stats

                    else
                        Game
            in
            ( { model | events = NoEvent, gameStatus = gameResolve, screen = showStats }, Cmd.none )

        OnVisibilityChange visibility ->
            case visibility of
                Visible ->
                    ( model, getUpdate )

                _ ->
                    ( model, Cmd.none )

        ShowConfiguration ->
            ( { model | screen = Config }, Cmd.none )

        ShowGame ->
            ( { model | screen = Game }, Cmd.none )

        ShowHowTo ->
            ( { model | screen = HowTo }, Cmd.none )

        ShowStatistics ->
            ( { model | screen = Stats }, Cmd.none )

        TickTick time ->
            ( { model | currectTime = time }, Cmd.none )

        ToggleColorBlind ->
            let
                toggleStatus : Bool
                toggleStatus =
                    not model.gameState.configuration.colorBlind

                state : GameState
                state =
                    model.gameState

                config : Configuration
                config =
                    model.gameState.configuration

                newConfig : Configuration
                newConfig =
                    { config | colorBlind = toggleStatus }

                newState : GameState
                newState =
                    { state | configuration = newConfig }
            in
            ( { model | gameState = newState }, setStorage newState )

        ToggleDarkMode ->
            let
                toggleStatus : Bool
                toggleStatus =
                    not model.gameState.configuration.darkMode

                state : GameState
                state =
                    model.gameState

                config : Configuration
                config =
                    model.gameState.configuration

                newConfig : Configuration
                newConfig =
                    { config | darkMode = toggleStatus }

                newState : GameState
                newState =
                    { state | configuration = newConfig }
            in
            ( { model | gameState = newState }, setStorage newState )

        ToggleHardMode ->
            let
                toggleStatus : Bool
                toggleStatus =
                    not model.gameState.configuration.hardMode

                state : GameState
                state =
                    model.gameState

                config : Configuration
                config =
                    model.gameState.configuration

                newConfig : Configuration
                newConfig =
                    { config | hardMode = toggleStatus }

                newState : GameState
                newState =
                    { state | configuration = newConfig }
            in
            ( { model | gameState = newState }, setStorage newState )

        Update ->
            ( { model | gameStatus = Ongoing }
            , Cmd.batch
                [ setStorage
                    { configuration = model.gameState.configuration
                    , firstTime = False
                    , gameNum = model.game
                    , guessList = guessListsStorage model.guessList
                    , intent = model.intent
                    , lastUpdate = model.updatedAt
                    , statistics = model.gameState.statistics
                    }
                , Browser.Navigation.reloadAndSkipCache
                ]
            )

        WindowResized width height ->
            ( { model
                | height = height
                , width = width
              }
            , Cmd.none
            )

        Zone zone ->
            ( { model | zone = zone }, getTime )


statsGuesses : Int -> List { asserts : Float } -> List { asserts : Float }
statsGuesses intent guesses =
    guesses
        |> List.indexedMap
            (\index item ->
                if index == intent then
                    { item | asserts = item.asserts + 1 }

                else
                    item
            )


shareText : GameStatus -> Int -> Int -> Array (Array CharStatus) -> String
shareText gameStatus gameNum intent guess =
    let
        guessList : List (List CharStatus)
        guessList =
            Array.toList <| Array.map (\a -> Array.toList a) guess

        palabrita : String
        palabrita =
            "Palabrita "
                ++ String.fromInt (gameNum + 1)
                ++ " "
                ++ (if gameStatus == Win then
                        String.fromInt intent

                    else
                        "X"
                   )
                ++ "/6 \n \n"

        link : String
        link =
            "https://palabrita.com.ar"

        squares : Int -> List (List CharStatus) -> String
        squares i guesses =
            String.concat
                (guesses
                    |> List.indexedMap
                        (\index item ->
                            if index < i then
                                String.fromList
                                    (List.foldr
                                        (\c acc ->
                                            case c of
                                                Absent _ ->
                                                    '⬛' :: acc

                                                Correct _ ->
                                                    '🟩' :: acc

                                                Hidden _ ->
                                                    '⬛' :: acc

                                                Present _ ->
                                                    '🟨' :: acc
                                        )
                                        []
                                        item
                                    )
                                    ++ "\n"

                            else
                                ""
                        )
                )
    in
    palabrita ++ squares intent guessList ++ link



-- VALIDATORS


sanitizedList : List String
sanitizedList =
    List.map (\w -> String.map sanitizeChar w) (Array.toList words)


isWordValid : Array CharStatus -> Bool
isWordValid guess =
    List.member
        (String.toLower <|
            String.fromList
                (Array.toList
                    (Array.map
                        (\ch ->
                            case ch of
                                Absent c ->
                                    c

                                Correct c ->
                                    c

                                Hidden c ->
                                    c

                                Present c ->
                                    c
                        )
                        guess
                    )
                )
        )
        (List.map (\w -> String.map sanitizeChar w) (Array.toList words))


sanitizeChar : Char -> Char
sanitizeChar char =
    if 'á' == char then
        'a'

    else if 'é' == char then
        'e'

    else if 'í' == char then
        'i'

    else if 'ó' == char then
        'o'

    else if 'ú' == char then
        'u'

    else if 'ü' == char then
        'u'

    else
        char



-- Set Character CharStatus


mapChars : List Char -> Char -> Char -> CharStatus
mapChars wordChars inputChar wordChar =
    if inputChar == wordChar then
        Correct inputChar

    else if List.member inputChar wordChars then
        Present inputChar

    else if inputChar == ' ' then
        Hidden inputChar

    else
        Absent inputChar


charStatusChar : CharStatus -> Char
charStatusChar cs =
    case cs of
        Absent c ->
            c

        Correct c ->
            c

        Hidden c ->
            c

        Present c ->
            c


validateGuess : String -> Array CharStatus -> Array CharStatus
validateGuess word input =
    let
        -- normalize =
        --     String.toLower >> SE.removeAccents
        ( wordChars, inputChars ) =
            ( String.toList <| String.map sanitizeChar word
            , Array.toList (Array.map (\c -> charStatusChar c) input)
            )
    in
    Array.fromList
        (wordChars
            |> List.map2 (mapChars wordChars) inputChars
            |> handleCorrectDuplicates wordChars
            |> handleMisplacedDuplicates wordChars
        )


{-| Find correctly placed letters; for each, if there's only one occurence in the word,
then check for misplaced same letter in the guess and mark them as Handled.
-}
handleCorrectDuplicates : List Char -> List CharStatus -> List CharStatus
handleCorrectDuplicates wordChars guess =
    guess
        |> List.map
            (\letter ->
                case letter of
                    Present c ->
                        let
                            ( nbCharsInWord, nbCorrectInAttempt ) =
                                ( -- count number of this char in target word
                                  List.length (List.filter ((==) c) wordChars)
                                  -- number of already correct char for
                                , List.length (List.filter (letterIs Correct c) guess)
                                )
                        in
                        if nbCorrectInAttempt >= nbCharsInWord then
                            -- there's enough correct letters for this char already
                            Absent c

                        else
                            letter

                    _ ->
                        letter
            )


letterIs : (Char -> CharStatus) -> Char -> CharStatus -> Bool
letterIs build char =
    (==) (build char)


{-| If a word contains a single A, and you provide an guess with 3 As, you'll have 3
misplaced As while we only want one, ideally the first one, with others marked as Handled.
-}
handleMisplacedDuplicates : List Char -> List CharStatus -> List CharStatus
handleMisplacedDuplicates wordChars =
    List.foldl
        (\letter acc ->
            case letter of
                Present c ->
                    let
                        ( nbCharInWord, nbCharInAcc ) =
                            -- count number of this char in target word
                            ( List.length (List.filter ((==) c) wordChars)
                              -- number of already misplaced char for in accumulator
                            , List.length (List.filter (letterIs Present c) acc)
                            )
                    in
                    if nbCharInAcc >= nbCharInWord then
                        -- there's enough misplaced letters for this char already
                        acc ++ [ Absent c ]

                    else
                        acc ++ [ letter ]

                _ ->
                    acc ++ [ letter ]
        )
        []


validateKeyboard : Array (Array CharStatus) -> Array CharStatus -> Array CharStatus
validateKeyboard guesses keyboardRow =
    let
        keyboardChars : List Char
        keyboardChars =
            Array.toList (Array.map (\c -> charStatusChar c) keyboardRow)
    in
    Array.fromList
        (List.map (keyState (Array.toList <| Array.map Array.toList guesses)) keyboardChars)


keyState : List (List CharStatus) -> Char -> CharStatus
keyState guess char =
    if List.any (List.any (letterIs Correct char)) guess then
        Correct char

    else if List.any (List.any (letterIs Present char)) guess then
        Present char

    else if List.any (List.any (letterIs Absent char)) guess then
        Absent char

    else
        Hidden char


setKeyStatus : Array (Array CharStatus) -> KeyboardKeys -> KeyboardKeys
setKeyStatus guesses keyboard =
    { firstRow = validateKeyboard guesses keyboard.firstRow
    , secondRow = validateKeyboard guesses keyboard.secondRow
    , thirdRow = validateKeyboard guesses keyboard.thirdRow
    }


compareCharStatus : CharStatus -> CharStatus -> Order
compareCharStatus x y =
    case x of
        Absent _ ->
            GT

        Correct _ ->
            LT

        Present _ ->
            case y of
                Correct _ ->
                    GT

                _ ->
                    LT

        _ ->
            GT


checkGameStatus : Int -> Array CharStatus -> GameStatus
checkGameStatus intent guess =
    if
        Array.length
            (Array.filter
                (\g ->
                    case g of
                        Correct _ ->
                            True

                        _ ->
                            False
                )
                guess
            )
            == Array.length guess
    then
        Win

    else if intent == 5 then
        Lose

    else
        Ongoing



-- UTILS


guessListsStorage : Array (Array CharStatus) -> List String
guessListsStorage guessList =
    Array.toList <| Array.map (\a -> String.fromList <| Array.toList (Array.map (\c -> charStatusChar c) a)) guessList


storageGuessList : List String -> String -> Array (Array CharStatus)
storageGuessList guessList secret =
    Array.fromList <| List.map (\s -> validateGuess secret (Array.map (\c -> Hidden c) <| Array.fromList <| String.toList s)) guessList



-- SECRET WORD
-- Get Todays Word


getCurrentSecret : Int -> Array String -> String
getCurrentSecret index words =
    Maybe.withDefault "" (Array.get index words)



-- VIEW


view : Model -> Document Msg
view model =
    { body =
        [ Element.layoutWith
            { options =
                [ Element.focusStyle
                    { backgroundColor = Maybe.Nothing
                    , borderColor = Maybe.Nothing
                    , shadow = Maybe.Nothing
                    }
                ]
            }
            [ Background.color
                (if model.gameState.configuration.darkMode then
                    bgColor

                 else
                    fgColor
                )
            , width (fill |> maximum model.width)
            , height (fill |> maximum model.height)
            , centerX
            , Font.family
                [ Font.external
                    { name = "Roboto"
                    , url = "https://fonts.googleapis.com/css?family=Roboto"
                    }
                , Font.sansSerif
                ]
            , Font.color
                (if model.gameState.configuration.darkMode then
                    fgColor

                 else
                    bgColor
                )
            ]
          <|
            case model.screen of
                HowTo ->
                    Element.Lazy.lazy howToView model

                _ ->
                    Element.Lazy.lazy gameView model
        ]
    , title = "Palabrita"
    }


gameView : Model -> Element Msg
gameView model =
    column
        [ width (fill |> maximum model.width)
        , height (fill |> maximum model.height)
        , spacing 5
        , inFront <|
            if model.screen == Stats then
                statsView model

            else if model.screen == Config then
                configView model

            else
                Element.none
        , inFront <|
            if model.events == UpdateNeeded then
                updateView model

            else
                Element.none
        ]
        [ column [ width (fill |> maximum 500), height fill, centerX ]
            [ row
                [ padding 10
                , Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }
                , width (fill |> minimum model.width)
                , spaceEvenly
                , centerX
                ]
                [ Input.button [ alignLeft, width fill ] { label = text "❓", onPress = Just ShowHowTo }
                , el
                    [ Font.center
                    , centerX
                    , width fill
                    , Font.size 32
                    , Font.family
                        [ Font.external
                            { name = "Alfa Slab One"
                            , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                            }
                        , Font.sansSerif
                        ]
                    ]
                  <|
                    text "Palabrita"
                , row [ width fill, spacing 10 ]
                    [ Input.button [ alignRight ] { label = text "📊", onPress = Just ShowStatistics }
                    , Input.button [ alignRight ] { label = text "⚙️", onPress = Just ShowConfiguration }
                    ]
                ]
            , column
                [ centerX
                , padding 10
                , spacing 10
                , height fill
                , width fill
                , inFront <|
                    case model.events of
                        ShowToast toastText ->
                            el
                                [ padding 10
                                , centerX
                                , Font.center
                                , Background.color
                                    (if model.gameState.configuration.darkMode then
                                        fgColor

                                     else
                                        bgColor
                                    )
                                , Font.color
                                    (if model.gameState.configuration.darkMode then
                                        bgColor

                                     else
                                        fgColor
                                    )
                                , Border.rounded 5
                                , moveDown 25
                                ]
                            <|
                                text toastText

                        _ ->
                            Element.none
                ]
                [ gameBoardView model
                , keyboardView model
                ]
            , row [ centerX, Font.center, spacing 10, paddingEach { bottom = 5, left = 0, right = 0, top = 0 }, centerY ]
                [ newTabLink [ alignBottom, centerX, Font.center, Font.size 12, Font.underline ] { label = text "Hecho en Elm", url = "https://elm-lang.org" }
                , newTabLink [ alignBottom, centerX, Font.center, Font.size 12, Font.underline ] { label = text "Analítica", url = "https://plausible.palabrita.com.ar/palabrita.com.ar" }
                , newTabLink [ alignBottom, centerX, Font.center, Font.size 12, Font.underline ] { label = text "Twitter", url = "https://twitter.com/holapalabrita" }
                ]
            ]
        ]


gameBoardView : Model -> Element Msg
gameBoardView model =
    column [ centerX, centerY, spacing 4, width (fill |> maximum 350), height (fill |> maximum 420) ]
        [ gameRow model.gameState.configuration.colorBlind <| Maybe.withDefault Array.empty (Array.get 0 model.guessList)
        , gameRow model.gameState.configuration.colorBlind <| Maybe.withDefault Array.empty (Array.get 1 model.guessList)
        , gameRow model.gameState.configuration.colorBlind <| Maybe.withDefault Array.empty (Array.get 2 model.guessList)
        , gameRow model.gameState.configuration.colorBlind <| Maybe.withDefault Array.empty (Array.get 3 model.guessList)
        , gameRow model.gameState.configuration.colorBlind <| Maybe.withDefault Array.empty (Array.get 4 model.guessList)
        , gameRow model.gameState.configuration.colorBlind <| Maybe.withDefault Array.empty (Array.get 5 model.guessList)
        ]


gameRow : Bool -> Array CharStatus -> Element Msg
gameRow highContrast array =
    row [ spacing 4, centerX, width (fill |> maximum 330), height (fill |> maximum 62) ]
        [ tile highContrast <| Maybe.withDefault (Hidden ' ') (Array.get 0 array)
        , tile highContrast <| Maybe.withDefault (Hidden ' ') (Array.get 1 array)
        , tile highContrast <| Maybe.withDefault (Hidden ' ') (Array.get 2 array)
        , tile highContrast <| Maybe.withDefault (Hidden ' ') (Array.get 3 array)
        , tile highContrast <| Maybe.withDefault (Hidden ' ') (Array.get 4 array)
        ]


tile : Bool -> CharStatus -> Element Msg
tile highContrast cs =
    let
        statusColor : List (Element.Attr decorative msg)
        statusColor =
            case cs of
                Absent _ ->
                    [ Border.color absentColor, Background.color absentColor, Font.color fgColor ]

                Correct _ ->
                    [ Border.color
                        (if highContrast then
                            correctContrastColor

                         else
                            correctColor
                        )
                    , Background.color
                        (if highContrast then
                            correctContrastColor

                         else
                            correctColor
                        )
                    , Font.color fgColor
                    ]

                Hidden _ ->
                    [ Border.color hiddenColor, Background.color (Element.rgba255 0 0 0 0) ]

                Present _ ->
                    [ Border.color
                        (if highContrast then
                            presentContrastColor

                         else
                            presentColor
                        )
                    , Background.color
                        (if highContrast then
                            presentContrastColor

                         else
                            presentColor
                        )
                    , Font.color fgColor
                    ]
    in
    el
        ([ width (fill |> maximum 62), height (fill |> maximum 62), Border.width 2 ]
            ++ statusColor
        )
    <|
        el [ Font.size 32, Font.center, centerX, centerY ] <|
            text <|
                String.toUpper <|
                    String.fromChar <|
                        charStatusChar cs



-- UPDATE VIEW


updateView : Model -> Element Msg
updateView model =
    column
        [ spacing 10
        , centerX
        , alignBottom
        , padding 5
        , Border.rounded 10
        , Border.width 1
        , Background.color
            (if model.gameState.configuration.darkMode then
                bgColor

             else
                fgColor
            )
        , width (fill |> maximum 350)
        , moveUp 25
        ]
        [ row [ centerX, width fill, padding 10 ]
            [ el
                [ Font.center
                , Font.size 26
                , centerX
                , width fill
                , Font.family
                    [ Font.external
                        { name = "Alfa Slab One"
                        , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                        }
                    , Font.sansSerif
                    ]
                ]
              <|
                text "Actualización 🥳"
            ]
        , paragraph [ paddingXY 25 10, Font.size 18, centerX, spacing 5 ]
            [ el [] <| text "Hay una nueva versión disponible. "
            , el [] <| text "Actualiza para tener una mejor experiencia!"
            ]
        , row [ width fill, alignBottom, centerX ]
            [ el [ alignLeft, Font.center, width <| fillPortion 2, padding 20 ] <| Input.button [ centerX, centerY, Background.color hiddenColor, Border.rounded 10, width (fill |> minimum 65), height (fill |> minimum 58) ] { label = text "Cancelar", onPress = Just CancelUpdate }
            , el [ alignRight, Font.center, width <| fillPortion 2, padding 20 ] <|
                Input.button
                    [ centerX
                    , centerY
                    , Background.color
                        (if model.gameState.configuration.colorBlind then
                            correctContrastColor

                         else
                            correctColor
                        )
                    , Border.rounded 10
                    , width (fill |> minimum 65)
                    , height (fill |> minimum 58)
                    ]
                    { label = text "Actualizar", onPress = Just Update }
            ]
        ]



-- KEYBOARD VIEW


keyboardView : Model -> Element Msg
keyboardView model =
    column [ spacing 5, centerX, Font.center, alignBottom, width (fill |> maximum 500), height (fill |> maximum 200) ]
        [ row [ spacing 5, centerX, width (fill |> maximum 480), height (fill |> minimum 58) ] <| List.map (\ct -> Input.button [ width (fill |> maximum 43), height (fill |> maximum 58), keyStatusColor model.gameState.configuration.colorBlind <| ct, Font.color fgColor, Border.rounded 5, Font.center ] { label = text <| fromChar <| Char.toUpper <| charStatusChar ct, onPress = Just (Character (Char.toLower <| charStatusChar ct)) }) <| Array.toList model.keyboard.firstRow
        , row [ spacing 5, centerX, width (fill |> maximum 432), height (fill |> minimum 58) ] <| List.map (\ct -> Input.button [ width (fill |> maximum 43), height (fill |> maximum 58), keyStatusColor model.gameState.configuration.colorBlind <| ct, Font.color fgColor, Border.rounded 5, Font.center ] { label = text <| fromChar <| Char.toUpper <| charStatusChar ct, onPress = Just (Character (Char.toLower <| charStatusChar ct)) }) <| Array.toList model.keyboard.secondRow
        , row [ spacing 5, centerX, width (fill |> maximum 486), height (fill |> minimum 58) ] <| enterButton :: (List.map (\ct -> Input.button [ width (fill |> maximum 43), height (fill |> maximum 58), keyStatusColor model.gameState.configuration.colorBlind <| ct, Font.color fgColor, Border.rounded 5, Font.center ] { label = text <| fromChar <| Char.toUpper <| charStatusChar ct, onPress = Just (Character (Char.toLower <| charStatusChar ct)) }) <| Array.toList model.keyboard.thirdRow) ++ [ backspaceButton ]
        ]


keyStatusColor : Bool -> CharStatus -> Element.Attr decorative msg
keyStatusColor highContrast status =
    case status of
        Absent _ ->
            Background.color absentColor

        Correct _ ->
            Background.color
                (if highContrast then
                    correctContrastColor

                 else
                    correctColor
                )

        Hidden _ ->
            Background.color keyboardColor

        Present _ ->
            Background.color
                (if highContrast then
                    presentContrastColor

                 else
                    presentColor
                )


enterButton : Element Msg
enterButton =
    Input.button [ width (fill |> maximum 65), height (fill |> maximum 58), Background.color specialKeyColor, Font.color fgColor, Border.rounded 5, Font.center ] { label = text <| "ENTER", onPress = Just Enter }


backspaceButton : Element Msg
backspaceButton =
    Input.button [ width (fill |> maximum 65), height (fill |> maximum 58), Background.color specialKeyColor, Font.color fgColor, Border.rounded 5, Font.center ] { label = text <| "⌫", onPress = Just Backspace }



-- HOWTO


howToView : Model -> Element Msg
howToView model =
    column
        [ width (fill |> maximum 500)
        , height (fill |> minimum model.height)
        , centerX
        ]
        [ row
            [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }
            , width (fill |> maximum 500)
            , spaceEvenly
            , centerX
            , padding 10
            , centerX
            ]
            [ el [ width fill ] Element.none
            , el
                [ centerX
                , width fill
                , Font.size 26
                , Font.center
                , Font.family
                    [ Font.external
                        { name = "Alfa Slab One"
                        , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                        }
                    , Font.sansSerif
                    ]
                ]
              <|
                text "COMO JUGAR"
            , Input.button [ alignRight, Font.alignRight, width fill ] { label = text "❌", onPress = Just ShowGame }
            ]
        , column
            [ spacing 10
            , Font.size 14
            , Font.alignLeft
            , alignLeft
            , padding 25
            ]
            [ paragraph [] [ text "Adivina la PALABRITA en 6 intentos." ]
            , paragraph [] [ text "Cada intento debe ser una palabra válida* de 5 letras. Presiona ENTER para enviar el intento." ]
            , paragraph [] [ text "Luego de cada intento, un color indicará que tan cerca estás de adivinar la PALABRITA." ]
            , paragraph [ Font.italic ] [ text ("* Que pertenezca a nuestra lista de " ++ String.fromInt (Array.length words) ++ " palabras.") ]
            ]
        , el [ Font.bold, Font.size 18, padding 10 ] <| text "Ejemplos"
        , column
            [ centerX
            , width (fill |> maximum 450)
            , height (fill |> maximum 520)
            , padding 25
            , spacing 10
            ]
            [ gameRow model.gameState.configuration.colorBlind <| Array.fromList [ Hidden 'H', Hidden 'A', Correct 'B', Hidden 'L', Hidden 'A' ]
            , paragraph [ Font.size 14 ]
                [ el [] <| text "La letra B esta en la"
                , el
                    [ Font.family
                        [ Font.external
                            { name = "Alfa Slab One"
                            , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                            }
                        , Font.sansSerif
                        ]
                    ]
                  <|
                    text " Palabrita "
                , el [] <| text "y en lugar correcto."
                ]
            , gameRow model.gameState.configuration.colorBlind <| Array.fromList [ Hidden 'C', Hidden 'A', Hidden 'R', Present 'T', Hidden 'A' ]
            , paragraph [ Font.size 14 ]
                [ el [] <| text "La letra T esta en la"
                , el
                    [ Font.family
                        [ Font.external
                            { name = "Alfa Slab One"
                            , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                            }
                        , Font.sansSerif
                        ]
                    ]
                  <|
                    text " Palabrita "
                , el [] <| text "pero no es el lugar correcto."
                ]
            , gameRow model.gameState.configuration.colorBlind <| Array.fromList [ Hidden 'T', Absent 'R', Hidden 'A', Hidden 'T', Hidden 'A' ]
            , paragraph [ Font.size 14 ]
                [ el [] <| text "La letra R no esta en la"
                , el
                    [ Font.family
                        [ Font.external
                            { name = "Alfa Slab One"
                            , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                            }
                        , Font.sansSerif
                        ]
                    ]
                  <|
                    text " Palabrita"
                , el [] <| text "."
                ]
            ]
        , column [ spacing 10, centerX, alignBottom ]
            [ el [ centerX, Font.size 14 ] <| text "Hay una nueva Palabrita cada día!"
            , row [ Font.size 14 ]
                [ el [] <| text "También podes jugar sin parar en "
                , newTabLink
                    [ centerX
                    , Font.size 14
                    , Font.underline
                    , Font.family
                        [ Font.external
                            { name = "Alfa Slab One"
                            , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                            }
                        , Font.sansSerif
                        ]
                    ]
                    { label = text "Palabritas", url = "https://otra.palabrita.com.ar" }
                ]
            ]
        , paragraph [ alignBottom, centerX, Font.center, padding 10, Font.size 14 ]
            [ el
                [ Font.family
                    [ Font.external
                        { name = "Alfa Slab One"
                        , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                        }
                    , Font.sansSerif
                    ]
                ]
              <|
                text "Palabrita"
            , el [] <| text " esta inspirado en "
            , newTabLink [ Font.underline ] { label = text "Wordle", url = "https://www.nytimes.com/games/wordle" }
            ]
        ]


configView : Model -> Element Msg
configView model =
    column
        [ spacing 10
        , centerX
        , alignBottom
        , padding 5
        , Border.rounded 10
        , Border.width 1
        , Background.color
            (if model.gameState.configuration.darkMode then
                bgColor

             else
                fgColor
            )
        , width (fill |> maximum 500)
        , moveUp 25
        ]
        [ row
            [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }
            , width (fill |> maximum 500)
            , spaceEvenly
            , centerX
            , padding 10
            , centerX
            ]
            [ el [ width fill ] Element.none
            , el
                [ centerX
                , width fill
                , Font.size 26
                , Font.center
                , Font.family
                    [ Font.external
                        { name = "Alfa Slab One"
                        , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                        }
                    , Font.sansSerif
                    ]
                ]
              <|
                text "CONFIGURACIÓN"
            , Input.button [ alignRight, Font.alignRight, width fill ] { label = text "❌", onPress = Just ShowGame }
            ]
        , column
            [ centerX
            , width (fill |> maximum 350)
            , height (fill |> maximum 420)
            , padding 15
            , spacing 25
            ]
            [ row [ width fill, transparent True ]
                [ el [ alignLeft, width fill ] <| text "Modo Dificíl"
                , Input.button [ alignRight, Font.alignRight ]
                    { label =
                        if model.gameState.configuration.hardMode then
                            text "🟢"

                        else
                            text "🔴"
                    , onPress = Just ToggleHardMode
                    }
                ]
            , row [ width fill ]
                [ el [ alignLeft, width fill ] <| text "Modo Oscuro"
                , el [ alignRight ] <|
                    Input.checkbox [ alignRight, Font.alignRight ] <|
                        { checked = model.gameState.configuration.darkMode
                        , icon =
                            toggleCheckboxWidget
                                { offColor = keyboardColor
                                , onColor =
                                    if model.gameState.configuration.colorBlind then
                                        correctContrastColor

                                    else
                                        correctColor
                                , sliderColor = fgColor
                                , toggleHeight = 28
                                , toggleWidth = 60
                                }
                        , label = Input.labelHidden "Activar/Descativar Modo Oscuro"
                        , onChange = always ToggleDarkMode
                        }
                ]
            , row [ width fill ]
                [ el [ alignLeft, width fill ] <| text "Alto Contraste"
                , el [ alignRight ] <|
                    Input.checkbox [ alignRight ] <|
                        { checked = model.gameState.configuration.colorBlind
                        , icon =
                            toggleCheckboxWidget
                                { offColor = keyboardColor
                                , onColor =
                                    if model.gameState.configuration.colorBlind then
                                        correctContrastColor

                                    else
                                        correctColor
                                , sliderColor = fgColor
                                , toggleHeight = 28
                                , toggleWidth = 60
                                }
                        , label = Input.labelHidden "Activar/Descativar Alto Contraste"
                        , onChange = always ToggleColorBlind
                        }
                ]
            ]
        , column
            [ centerX
            , width (fill |> maximum 350)
            , height (fill |> maximum 420)
            , padding 15
            , spacing 10
            ]
            [ row [ width fill, alignRight ]
                [ el [ alignLeft, width fill ] <| text "Contacto"
                , row [ spaceEvenly ]
                    [ newTabLink [ Font.underline, padding 10, Border.widthEach { bottom = 0, left = 0, right = 1, top = 0 } ] { label = text "email", url = "mailto:hola@palabrita.com.ar?subject=Palabrita " ++ String.fromInt (model.game + 1) }
                    , newTabLink [ Font.underline, padding 10 ] { label = text "twitter", url = "https://twitter.com/holapalabrita" }
                    ]
                ]
            ]
        ]


statsView : Model -> Element Msg
statsView model =
    column
        [ spacing 25
        , centerX
        , centerY
        , padding 15
        , Border.rounded 10
        , Border.width 1
        , Background.color
            (if model.gameState.configuration.darkMode then
                bgColor

             else
                fgColor
            )
        , width (fill |> maximum 500)
        , height (fill |> maximum 550)
        ]
        [ row [ centerX, width fill ]
            [ el [ width fill ] <| Element.none
            , el
                [ Font.center
                , Font.size 26
                , centerX
                , width fill
                , Font.family
                    [ Font.external
                        { name = "Alfa Slab One"
                        , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                        }
                    , Font.sansSerif
                    ]
                ]
              <|
                text "ESTADÍSTICAS"
            , Input.button [ width fill, alignRight, Font.alignRight ] { label = text "❌", onPress = Just ShowGame }
            ]
        , wrappedRow [ spacing 20, width fill ]
            [ column [ centerX, Font.center, alignTop ]
                [ el [ centerX, Font.size 28 ] <| text <| String.fromInt model.statistics.gamesPlayed
                , el [ centerX, Font.size 14 ] <| text "Juegos"
                ]
            , column [ centerX, Font.center, alignTop ]
                [ el [ centerX, Font.size 28 ] <| text <| String.fromInt model.statistics.winPercentage
                , el [ centerX, Font.size 14 ] <| text "% Aciertos"
                ]
            , column [ centerX, Font.center, alignTop ]
                [ el [ centerX, Font.size 28 ] <| text <| String.fromInt model.statistics.currentStreak
                , el [ centerX, Font.size 14 ] <| text "Racha"
                , el [ centerX, Font.size 14 ] <| text "Actual"
                ]
            , column [ centerX, Font.center, alignTop ]
                [ el [ centerX, Font.size 28 ] <| text <| String.fromInt model.statistics.maxStreak
                , el [ centerX, Font.size 14 ] <| text "Racha"
                , el [ centerX, Font.size 14 ] <| text "Máxima"
                ]
            ]
        , el [ Font.center, padding 20, centerX, width (fill |> maximum 350) ] <| Element.html <| chartView model model.statistics.guesses
        , row [ width fill, alignBottom ]
            [ column [ alignLeft, Font.center, width <| fillPortion 2, Border.widthEach { bottom = 0, left = 0, right = 1, top = 0 }, spacing 5 ]
                [ el [ centerX, Font.center, Font.size 18 ] <| text "Próxima"
                , el
                    [ centerX
                    , Font.center
                    , Font.size 18
                    , Font.family
                        [ Font.external
                            { name = "Alfa Slab One"
                            , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                            }
                        , Font.sansSerif
                        ]
                    ]
                  <|
                    text "Palabrita"
                , el [ centerX, Font.center, padding 5, Font.size 24 ] <| text <| getTimeStringToNextGame model.zone model.currectTime
                ]
            , if model.gameStatus == Ongoing then
                el [ alignRight, Font.center, width <| fillPortion 2, padding 20 ] <|
                    Input.button
                        [ centerX
                        , centerY
                        , Background.color hiddenColor
                        , Border.rounded 10
                        , width (fill |> minimum 65)
                        , height (fill |> minimum 58)
                        , padding 5
                        ]
                        { label = text "Compartir 🔗", onPress = Just DoNothing }

              else
                el [ alignRight, Font.center, width <| fillPortion 2, padding 20 ] <|
                    Input.button
                        [ centerX
                        , centerY
                        , Background.color
                            (if model.gameState.configuration.colorBlind then
                                correctContrastColor

                             else
                                correctColor
                            )
                        , Border.rounded 10
                        , width (fill |> minimum 65)
                        , height (fill |> minimum 58)
                        ]
                        { label = text "Compartir 🔗", onPress = Just Copy }
            ]
        , if model.gameStatus == Win || model.gameStatus == Lose then
            paragraph [ centerX, Font.center, Font.size 14 ]
                [ newTabLink [ centerY, centerX, Font.center, Font.underline ]
                    { label =
                        paragraph []
                            [ el [] <| text "La respuesta es "
                            , el
                                [ Font.family
                                    [ Font.external
                                        { name = "Alfa Slab One"
                                        , url = "https://fonts.googleapis.com/css?family=Alfa Slab One"
                                        }
                                    , Font.sansSerif
                                    ]
                                ]
                              <|
                                text (String.toUpper model.secret)
                            , el [] <| text ", haz click para ver la definición!"
                            ]
                    , url = "https://dle.rae.es/?w=" ++ model.secret
                    }
                ]

          else
            Element.none
        ]


chartView : Model -> List { asserts : Float } -> Html Msg
chartView model data =
    C.chart
        [ CA.width 300 -- Sets width dimension of chart
        , CA.height 150 -- Sets height dimension of chart
        ]
        [ C.xLabels
            [ CA.fontSize 14
            , CA.ints
            , CA.color
                (if model.gameState.configuration.darkMode then
                    "white"

                 else
                    "black"
                )
            ]
        , C.yLabels
            [ CA.withGrid
            , CA.fontSize 14
            , CA.ints
            , CA.color
                (if model.gameState.configuration.darkMode then
                    "white"

                 else
                    "black"
                )
            ]
        , C.labelAt .min
            CA.middle
            [ CA.moveLeft 25
            , CA.rotate 90
            , CA.fontSize 14
            , CA.color
                (if model.gameState.configuration.darkMode then
                    "white"

                 else
                    "black"
                )
            ]
            [ S.text "Aciertos" ]
        , C.labelAt CA.middle
            .min
            [ CA.moveDown 30
            , CA.fontSize 14
            , CA.color
                (if model.gameState.configuration.darkMode then
                    "white"

                 else
                    "black"
                )
            ]
            [ S.text "Fila" ]
        , C.labelAt .max
            .max
            [ CA.moveLeft 8
            , CA.moveUp 15
            , CA.alignRight
            , CA.fontSize 14
            , CA.color
                (if model.gameState.configuration.darkMode then
                    "white"

                 else
                    "black"
                )
            ]
            [ S.text "Distribución de Aciertos" ]
        , C.bars [ CA.margin 0.8, CA.roundTop 0.3 ]
            [ C.bar .asserts
                [ CA.striped [ CA.spacing 6 ]
                , CA.color
                    (if model.gameState.configuration.darkMode then
                        "white"

                     else
                        "black"
                    )
                ]
            ]
            data
        ]


winToastText : Int -> String
winToastText intent =
    case intent of
        0 ->
            "Increíble! 🥵"

        1 ->
            "Impresionante! 🤩"

        2 ->
            "Excelente! 😎"

        3 ->
            "Genial! 😁"

        4 ->
            "Buen Trabajo! 😅"

        5 ->
            "Lo lograste! \u{1F972}"

        _ ->
            ""


toggleCheckboxWidget : { offColor : Color, onColor : Color, sliderColor : Color, toggleHeight : Int, toggleWidth : Int } -> Bool -> Element msg
toggleCheckboxWidget { offColor, onColor, sliderColor, toggleHeight, toggleWidth } checked =
    let
        pad : Int
        pad =
            3

        sliderSize : Int
        sliderSize =
            toggleHeight - 2 * pad
    in
    el
        [ Background.color <|
            if checked then
                onColor

            else
                offColor
        , width <| px <| toggleWidth
        , height <| px <| toggleHeight
        , Border.rounded 14
        , inFront <|
            el [ height fill ] <|
                el
                    [ Background.color sliderColor
                    , Border.rounded <| sliderSize // 2
                    , width <| px <| sliderSize
                    , height <| px <| sliderSize
                    , centerY
                    , moveRight (toFloat pad)
                    , htmlAttribute <|
                        HA.style "transition" ".4s"
                    , htmlAttribute <|
                        if checked then
                            let
                                translation : String
                                translation =
                                    (toggleWidth - sliderSize - pad)
                                        |> String.fromInt
                            in
                            HA.style "transform" <| "translateX(" ++ translation ++ "px)"

                        else
                            HA.class ""
                    ]
                <|
                    text ""
        ]
    <|
        text ""


withTooltip : String -> Element msg -> Element msg
withTooltip str element =
    el
        [ Element.inFront <|
            el
                [ width fill
                , height fill
                , Element.transparent True
                , Element.mouseOver [ Element.transparent False ]
                , Element.onRight (tooltip str)
                ]
                Element.none
        ]
        element


tooltip : String -> Element msg
tooltip str =
    el
        [ Background.color (Element.rgb 0 0 0)
        , Font.color (Element.rgb 1 1 1)
        , Element.moveUp 32
        , Element.moveLeft 32
        , padding 4
        , Border.rounded 5
        , Font.size 14
        , Border.shadow
            { blur = 6, color = Element.rgba 0 0 0 0.32, offset = ( 0, 3 ), size = 0 }
        ]
        (text str)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Browser.Events.onResize WindowResized
        , Browser.Events.onKeyDown keyDecoder
        , Browser.Events.onVisibilityChange OnVisibilityChange
        , Time.every (60 * 60 * 1000) CheckDate
        , Time.every 1000 TickTick
        , case model.events of
            ShowToast _ ->
                Time.every 1500 HideToast

            _ ->
                Sub.none
        ]
