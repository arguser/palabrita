module Colors exposing (absentColor, bgColor, borderColor, correctColor, correctContrastColor, fgColor, hiddenColor, keyboardColor, presentColor, presentContrastColor, specialKeyColor)

import Element exposing (Color)



-- COLORS


bgColor : Color
bgColor =
    Element.rgba255 18 18 19 1


borderColor : Color
borderColor =
    Element.rgba255 58 58 60 1


fgColor : Color
fgColor =
    Element.rgba255 245 245 245 1


hiddenColor : Color
hiddenColor =
    Element.rgba255 58 58 60 1


presentColor : Color
presentColor =
    Element.rgba255 181 159 59 1


correctColor : Color
correctColor =
    Element.rgba255 83 141 78 1


presentContrastColor : Color
presentContrastColor =
    Element.rgba255 133 192 249 1


correctContrastColor : Color
correctContrastColor =
    Element.rgba255 245 121 58 1


absentColor : Color
absentColor =
    Element.rgba255 58 58 60 1


keyboardColor : Color
keyboardColor =
    Element.rgba255 129 131 132 1


specialKeyColor : Color
specialKeyColor =
    Element.rgba255 24 150 231 1
