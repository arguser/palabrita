# Changelog

## [1.13.3] - 2022-03-08

### Modificado

- Service Worker

## [1.13.2] - 2022-03-07

### Modificado

- Solucion en vista de estadisticas

## [1.13.1] - 2022-03-04

### Modificado

- Mejoras de rendimiento

## [1.13.0] - 2022-02-27

### Agregado

- Elm-Review

### Modificado

- Modelado de datos
- Validacion de conjetura

## [1.12.0] - 2022-02-25

### Modificado

- Aumento de lista de palabras

## [1.11.4] - 2022-02-17

### Agregado

- Evitar traduccion automatica

## [1.11.3] - 2022-02-16

### Agregado

- Emojis en toasts

## [1.11.2] - 2022-02-15

### Modificado

- Fuente
- Colores
- Verificación de caracteres repetidos
- Barras de visualizacion de estadistica

## [1.11.1] - 2022-02-12

### Modificado

- Mensaje en Toast al copiar texto para compartir

## [1.11.0] - 2022-02-11

### Agregado

- Aplicación Web Progresiva

## [1.10.5] - 2022-02-09

### Modificado

- Analiticas publicas
- Mejora de filtrado de palabras
- Colores en archivo separado
- Mejoras en mensaje toast
- Cambio de imagen de banner de Twitter

## [1.10.4] - 2022-02-06

### Modificado

- Desactivar botón de compartir si el juego no termino

## [1.10.3] - 2022-02-05

### Agregado

- Medios de contacto
- Mejora visual en los botones de configuración

### Modificado

- Mejora en contraste
- Mejora en texto para compartir

## [1.10.2] - 2022-02-04

### Agregado

- Verificar actualización en cambio de foco

### Modificado

- Color de fuente

## [1.10.1] - 2022-02-04

### Modificado

- Ocultar prestaciones no implementadas
- Espaciado de la vista de configuración
- Espaciado de la vista de estadistica

## [1.10.0] - 2022-02-04

### Agregado

- Vista de Configuración
- Modo Oscuro

## [1.9.1] - 2022-02-03

### Modificado

- Arreglos en el refresco de datos

## [1.9.0] - 2022-02-02

### Agregado

- Analitica enfocada en la privacidad: Plausible
- Metadatos

### Modificado

- Mejoras en el manejo de nueva version

## [1.8.0] - 2022-02-01

### Agregado

- Dominio formal: palabrita.com.ar
- Mecanismo de notificación de nueva version

### Modificado

- Nueva fecha referencial de inicio

## [1.7.0] - 2022-01-31

### Agregado

- Palabras con ñ
- Palabras con tildes
- Sanitizador de palabras

### Modificado

- Mejoras en validadores

## [1.6.1] - 2022-01-29

### Modificado

- Utilizar zona horaria del cliente
- Estilado en vinculos

### Removido

- Archivos de referencia

## [1.6.0] - 2022-01-29

### Agregado

- Palabra Invalida

## [1.5.1] - 2022-01-26

### Modificado

- Mejoras en teclado visual

## [1.5.0] - 2022-01-25

### Agregado

- Archivo index
- Icono
- Diseño adaptable
- Tiempo para el proximo juego
- Mostrar palabra al terminar juego
- Integración Continua de Gitlab
- Gitlab Pages

### Modificado

- Optimizaciones

## [1.4.1] - 2022-01-21

### Agregado

- Refrescar cuando hay una nueva palabra

## [1.4.0] - 2022-01-21

### Agregado

- Botón compartir

## [1.3.0] - 2022-01-21

### Agregado

- Estadisticas y grafico de estadisticas

## [1.2.0] - 2022-01-21

### Agregado

- Almacenamiento Local

## [1.2.0] - 2022-01-20

### Agregado

- Estado de Juego

## [1.1.1] - 2022-01-20

### Modificado

- Mejores en teclado

## [1.1.0] - 2022-01-20

### Agregado

- Teclado visual

## [1.0.1] - 2022-01-19

### Modificado

- Validación de palabra y conjetura

## [1.0.0] - 2022-01-18

### Agregado

- Lista de palabras de 5 caracteres

- Colores

- Archivo Lee me
